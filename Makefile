#!/usr/bin/make -f

 BUNDLE := bundle
 JEK    := $(BUNDLE) exec jekyll
 ROOT   := hercule:touheypro

build: -prepare
	$(JEK) build $(OPTS)
	find _site -type f -exec chmod 644 {} \;

preview: -prepare
	$(JEK) serve --drafts --watch $(OPTS)

show: build
	rsync -Prlt --delete --exclude=up/ _site/ "$(ROOT)"

 -prepare:
	$(BUNDLE) check || $(BUNDLE) install --path vendor/bundle

.PHONY: -prepare build preview show

# End of file.
